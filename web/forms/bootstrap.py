class BootStrapForm:
    bootstrap_class_exclude = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            if name in self.bootstrap_class_exclude:
                continue
            # field.widget.attrs['class'] = 'form-control'
            old_class = field.widget.attrs.get('class', '')
            field.widget.attrs['class'] = '{} form-control'.format(old_class)

            if name == 'confirm_password':
                field.widget.attrs['placeholder'] = '请重复密码'
            else:
                field.widget.attrs['placeholder'] = '请输入%s' % field.label
