from django import forms
from django.core.exceptions import ValidationError

from web import models
from web.forms.bootstrap import BootStrapForm
from web.forms.widgets import ColorRadioSelect


class ProjectModelForm(BootStrapForm, forms.ModelForm):
    bootstrap_class_exclude = ['color']

    class Meta:
        model = models.Project
        fields = ['name', 'color', 'desc']
        widgets = {
            'desc': forms.Textarea,
            'color': ColorRadioSelect(attrs={'class': 'color-radio'}),
        }

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request

    def clean_name(self):
        name = self.cleaned_data['name']
        # 判断当前项目名称是否已经存在。
        is_exists = models.Project.objects.filter(name=name, creator=self.request.lxyker.user).exists()
        if is_exists:
            raise ValidationError('项目已存在')

        # 判断当前用户是否还有额度创建项目
        # 现在创建了多少(count)项目数？
        count = models.Project.objects.filter(creator=self.request.lxyker.user).count()

        if count >= self.request.lxyker.price_policy.project_num:
            raise ValidationError('可创项目数量不足，请升级套餐！')
        return name
