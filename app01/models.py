from django.core.validators import RegexValidator
from django.db import models


class UserInfo(models.Model):
    username = models.CharField(verbose_name='用户名', max_length=32)
    email = models.EmailField(verbose_name='邮箱', max_length=64)
    mobile_phone = models.CharField(verbose_name='手机号', max_length=16,
                                    validators=[RegexValidator(r'^1[3,4,5,6,7,8]\d{9}$')])

