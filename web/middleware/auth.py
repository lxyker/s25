import datetime

from django.conf import settings
from django.shortcuts import redirect
from django.utils.deprecation import MiddlewareMixin

from web import models


class Lxyker:
    def __init__(self):
        self.user = None
        self.price_policy = None
        self.project = None


class AuthMiddleware(MiddlewareMixin):
    def process_request(self, request):
        """
        如果用户已经登录，那么request中有值
        :param request:
        :return:
        """
        request.lxyker = Lxyker()

        user_id = request.session.get('user_id', 0)
        user_object = models.UserInfo.objects.filter(id=user_id).first()
        request.lxyker.user = user_object

        # 白名单：没有登录也可以访问的URL
        """
        1、获取当前用户访问的URL
        2、检查这个URL是否在白名单中，如果在白名单中，就可以继续访问，否则判断是否登录
        """
        if request.path_info in settings.WHITE_REGEX_URL_LIST:
            return

        if not request.lxyker.user:
            return redirect('login')

        # 登录成功之后，访问后台管理时：获取当前用户所拥有的额度
        # 方式一：免费额度在交易记录中存储
        # """
        _object = models.Transaction.objects.filter(user=user_object, status=2).order_by(
            '-id').first()  # 获取当前用户ID值最大的交易记录（ID最大表示最近的交易）

        # 判断这个交易记录是否过期，如果过期，那就选择免费版作为_object
        current_datetime = datetime.datetime.now()
        if _object.end_datetime and _object.end_datetime < current_datetime:
            # 这个条件表示这条交易过期
            _object = models.Transaction.objects.filter(user=user_object, status=2, price_policy__category=1).order_by(
                'id').first()
        # request.transaction = _object
        request.lxyker.price_policy = _object.price_policy
        # """

        """
        # 方式二：免费的额度存储配置文件
        _object = models.Transaction.objects.filter(user=user_object, status=2).order_by('-id').first()
        
        if not _object:
            # 没有购买过
            request.price_policy = models.PricePolicy.objects.filter(category=1, title='个人免费版').first()
        else:
            # 付费
            if _object.end_datetime and _object.end_datetime < current_datetime:
                # 过期
                request.price_policy = models.PricePolicy.objects.filter(category=1, title='个人免费版').first()
            else:
                request.price_policy = _object.price_policy
        """

    def process_view(self, request, view, args, kwargs):
        # 判断URL是否以manage开头：
            # 如果是，就判断项目ID是否为自己创建或参与的
        if not request.path_info.startswith('/manage/'):
            return  # 不是以manage开头,就继续向下走

        project_id = kwargs.get('project_id')

        created_project = models.Project.objects.filter(creator=request.lxyker.user, id=project_id).first()
        if created_project:
            # 是自己创建的项目
            request.lxyker.project = created_project  # 记住这个项目,之后直接使用request.lxyker.project来判断是否进入项目
            return
        joined_project = models.ProjectUser.objects.filter(user=request.lxyker.user, id=project_id).first()
        if joined_project:
            # 是自己加入的项目
            request.lxyker.project = joined_project.project
            return

        # 走到这里表示既不是你创建的,也不是你加入的,那就直接让你重新查看project_list
        return redirect('project_list')
