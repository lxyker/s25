import hashlib
import uuid

from django.conf import settings


def md5(string):
    """
    md5加密
    :param string: 原生字符串
    :return: 加密后的字符串
    """
    # 直接使用django的SECRET_KEY作为盐
    hash_object = hashlib.md5(settings.SECRET_KEY.encode('utf-8'))
    hash_object.update(string.encode('utf-8'))
    return hash_object.hexdigest()


def uid(string):
    """给图片设置随机字符串名"""
    data = '{}-{}'.format(str(uuid.uuid4()), string)
    return md5(data)