from django.http import HttpResponse
from django.shortcuts import render, redirect

from utils.tencent.cos import delete_bucket
from web import models


def setting(request, project_id):
    """"""
    return render(request, 'web/setting.html')


def delete(request, project_id):
    """删除项目"""

    if request.method == 'GET':
        return render(request, 'web/setting_delete.html')

    project_name = request.POST.get('project_name')

    if request.lxyker.user != request.lxyker.project.creator:
        return render(request, 'web/setting_delete.html', {'error': '权限不够'})

    if not project_name or project_name != request.lxyker.project.name:
        return render(request, 'web/setting_delete.html', {'error': '项目名称不正确'})

    # 输入项目名成功则执行删除项目操作
    # 1、删除cos中的桶，2、删除数据库中的所有文件
    delete_bucket(request.lxyker.project.bucket, request.lxyker.project.region)
    models.Project.objects.filter(id=request.lxyker.project.id).delete()

    # 删除成功，跳转到项目列表页面
    return redirect('project_list')
